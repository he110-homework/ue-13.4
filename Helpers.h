#ifndef HOMEWORK_13_4_HELPERS_H
#define HOMEWORK_13_4_HELPERS_H

int sumPow(int first, int second)
{
    return pow(first + second, 2);
}

#endif //HOMEWORK_13_4_HELPERS_H
