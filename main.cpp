#include <iostream>
#include "Helpers.h"

int main() {
    std::cout << sumPow(1, 1) << std::endl;
    std::cout << sumPow(5, 2) << std::endl;
    std::cout << sumPow(2, 3) << std::endl;
    return 0;
}
